import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { TokenStorageService } from '../auth/token-storage.service';

const HOST = 'http://localhost:8765/';

@Injectable()
export class RolesService {
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient, private tokenStorage: TokenStorageService) {}
  
  getRoles(): Observable<any> {
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', this.tokenStorage.getToken());
    return this.http.get(HOST+"roles", this.httpOptions);
  }

  deleteRole(id:number): Observable<any> {
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', this.tokenStorage.getToken());
    return this.http.delete(HOST+"roles/"+id, this.httpOptions);
  }

  craeteRole(body: any): Observable<any> {
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', this.tokenStorage.getToken());
    let bodyJson = JSON.stringify(body);
    console.log(bodyJson);
    return this.http.post(HOST+"roles", bodyJson, this.httpOptions);
  }

  getRole(idRol: number): Observable<any> {
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', this.tokenStorage.getToken());
    return this.http.get(HOST+"roles/"+idRol, this.httpOptions);
  }

  updateRole(body: any, id: number): Observable<any> {
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', this.tokenStorage.getToken());
    let bodyJson = JSON.stringify(body);
    console.log(bodyJson);
    return this.http.put(HOST+"roles/"+id, bodyJson, this.httpOptions);
  }

  usersRelRole(id: number): Observable<any> {
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', this.tokenStorage.getToken());
    return this.http.get(HOST+"users/role/"+id, this.httpOptions);
  }
}