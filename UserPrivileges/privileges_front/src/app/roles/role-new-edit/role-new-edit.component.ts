import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { RolesService } from '../roles.service';
import { BodyReq } from './body-req';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-role-new-edit',
  templateUrl: './role-new-edit.component.html',
  styleUrls: ['./role-new-edit.component.css']
})
export class RoleNewEditComponent implements OnInit {
  id!: number;
  editMode = false;
  roleForm!: FormGroup;
  content!: string;
  @ViewChild("modalMensaje") modal!: ElementRef;
  mensaje = "";
  @ViewChild("modalMensajeErr") modalErr!: ElementRef;
  mensajeErr = "";

  constructor(private route: ActivatedRoute, 
    private rolesService: RolesService,
    private modalService: NgbModal,
    private router: Router) { }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.id = +params['id'];
      this.editMode = params['id'] != null;
      this.initForm();
    });
  }

  private loadRole(role: any){
      let name = role.name;
      let privilegeEdit = false;
      let privilegeCreate = false;

      for (let privilege of role.privileges) {
        if(privilege.name == "edit"){
          privilegeEdit = true;
        }
        if(privilege.name == "create"){
          privilegeCreate = true;
        }
      }

      this.roleForm = new FormGroup({
        'name': new FormControl(name, Validators.required),
        'privilegeEdit': new FormControl(privilegeEdit),
        'privilegeCreate': new FormControl(privilegeCreate),
      });
  }
  private initForm() {
    let name = '';
    let privilegeEdit: any;
    let privilegeCreate: any;
    var role: any; 

    if (this.editMode) {
      this.mensajeErr = "";
      this.rolesService.getRole(this.id).subscribe(
        data => {
          var respStr = JSON.stringify(data);
          role = JSON.parse(respStr);

          this.loadRole(role);
        },
        err => {
          this.mensajeErr = err.error.errors[0].message;
          this.modalService.open(this.modalErr);
          this.router.navigate(['/roles']);
        }
      );
    }

    this.roleForm = new FormGroup({
      'name': new FormControl(name, Validators.required),
      'privilegeEdit': new FormControl(privilegeEdit),
      'privilegeCreate': new FormControl(privilegeCreate),
    });
  }

  onSubmitCreate(){
    this.mensajeErr = "";
    var body:BodyReq = new BodyReq();
    body.setId(0);
    body.setName(this.roleForm.value['name']);
    body.addPrivilege(1, "read");
    if(this.roleForm.value['privilegeEdit'] == true){
      body.addPrivilege(2, "edit");
    }
    if(this.roleForm.value['privilegeCreate']){
      body.addPrivilege(3, "create");
    }
    
    this.rolesService.craeteRole(body).subscribe(
      data => {
        this.content = data;
        this.closeModal();
        this.router.navigate(['/roles']);
      },
      err => {
        this.mensajeErr = err.error.errors[0].message;
        this.modalService.open(this.modalErr);
      }
    );
  }

  closeModal(){
    this.modalService.dismissAll();
  }

  updateRoleCom(){
    this.mensajeErr = "";
    this.rolesService.usersRelRole(this.id).subscribe(
      data => {
        var respStr = JSON.stringify(data);
        var valid = JSON.parse(respStr);
        this.mensaje = "";
        if(valid.usersRel == true){
          this.mensaje = "Hay usuarios relacionados con este rol: la nueva configuración impactará a los usuarios relacionados.";
        }
        
      },
      err => {
        this.mensajeErr = err.error.errors[0].message;
        this.modalService.open(this.modalErr);
        this.router.navigate(['/roles']);
      }
    );
    this.modalService.open(this.modal);
  }

  confRoleCom(){
    this.mensajeErr = "";
    var body:BodyReq = new BodyReq();
    body.setName(this.roleForm.value['name']);
    body.addPrivilege(1, "read");
    if(this.roleForm.value['privilegeEdit'] == true){
      body.addPrivilege(2, "edit");
    }
    if(this.roleForm.value['privilegeCreate']){
      body.addPrivilege(3, "create");
    }
    
    this.rolesService.updateRole(body, this.id).subscribe(
      data => {
        this.content = data;
        this.closeModal();
        this.router.navigate(['/roles']);
      },
      err => {
        this.mensajeErr = err.error.errors[0].message;
        this.modalService.open(this.modalErr);
      }
    );

    this.mensaje = "";
  }
}
