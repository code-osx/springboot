export class Privilege {
    private id!: number;
    private name: string;

    constructor(id: number, name: string){
        this.id = id;
        this.name = name;
    }

    public setId(id: number): void {
        this.id = id;
    }

    public setName(name: string): void {
        this.name = name;
    }
}

export class BodyReq {
    private id!: number;
    private name!: string;
    private privileges: Privilege[] = [];

    public setId(id: number): void {
        this.id = id;
    }

    public setName(name: string): void {
        this.name = name;
    }

    public addPrivilege(id: number, name: string){
        var privilege = new Privilege(id, name);
        this.privileges.push(privilege)
    }

    constructor(){
    }

    
}