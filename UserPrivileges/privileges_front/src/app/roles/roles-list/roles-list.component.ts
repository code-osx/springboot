import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { RolesService } from '../roles.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-roles-list',
  templateUrl: './roles-list.component.html',
  styleUrls: ['./roles-list.component.css']
})
export class RolesListComponent implements OnInit {
  roles: any;
  isLoggedIn = false;
  content!: string;
  idDel!: number;
  @ViewChild("modalMensaje") modal!: ElementRef;
  mensaje = "";
  @ViewChild("modalMensajeErr") modalErr!: ElementRef;
  mensajeErr = "";

  constructor(
    private roleService: RolesService,
    private modalService: NgbModal) { }

  ngOnInit() {
    this.mensajeErr = "";
    this.roleService.getRoles().subscribe(
      data => {
        var respStr = JSON.stringify(data);
        const resp = JSON.parse(respStr);
        this.roles = resp._embedded.roleList;
      },
      err => {
        this.content = err;
        this.mensajeErr = err.error.errors[0].message;
        this.modalService.open(this.modalErr);
      }
    );
  }

  deleteRoleCom(id: number){
    this.mensaje = "";
    this.roleService.usersRelRole(id).subscribe(
      data => {
        var respStr = JSON.stringify(data);
        var valid = JSON.parse(respStr);
        if(valid.usersRel == true){
          this.mensaje = "Hay usuarios relacionados con este rol: la eliminación del rol impactará a los usuarios relacionados.";
        }
        
      },
      err => {
        this.content = err;
      }
    );

    this.modalService.open(this.modal);
    this.idDel = id;
  }

  closeModal(){
    this.modalService.dismissAll();
  }

  confRoleCom(){
    this.roleService.deleteRole(this.idDel).subscribe(
      data => {
        var respStr = JSON.stringify(data);
        const resp = JSON.parse(respStr);
        this.roles = resp._embedded.roleList;
      },
      err => {
        this.content = err;
      }
    );
    this.closeModal();
    window.location.reload();
  }
}
