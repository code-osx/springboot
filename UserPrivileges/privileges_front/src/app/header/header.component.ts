import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TokenStorageService } from '../auth/token-storage.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  isLoggedIn = false;

  constructor(private tokenStorage: TokenStorageService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    if (this.tokenStorage.getToken() != null) {
      this.isLoggedIn = true;
    } else{
      this.router.navigate(['/'], { relativeTo: this.route });
    }
  }

  onCloseSession(){
    this.tokenStorage.signOut();
    window.location.reload();
  }
}
