import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthComponent } from './auth/auth.component';
import { RoleNewEditComponent } from './roles/role-new-edit/role-new-edit.component';
import { RolesListComponent } from './roles/roles-list/roles-list.component';
import { RolesComponent } from './roles/roles.component';
import { UsersComponent } from './users/users.component';

const routes: Routes = [
  {
    path: "",
    component: AuthComponent
  },
  {
    path: "users",
    component: UsersComponent
  },
  {
    path: "roles",
    component: RolesComponent,
    children: [
      { path: '', component: RolesListComponent },
      { path: 'new', component: RoleNewEditComponent },
      { path: 'edit/:id', component: RoleNewEditComponent }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
