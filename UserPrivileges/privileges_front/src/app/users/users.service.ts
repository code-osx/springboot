import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { TokenStorageService } from '../auth/token-storage.service';

const HOST = 'http://localhost:8765/';

@Injectable()
export class UserService {
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient, private tokenStorage: TokenStorageService) {}

  getUsers(): Observable<any> {
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', this.tokenStorage.getToken());
    return this.http.get(HOST+"users/roles", this.httpOptions);
  }
}