import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TokenStorageService } from '../auth/token-storage.service';
import { UserService } from './users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  users: any;
  content!: string; 
  mySubscription: any;
  @ViewChild("modalMensajeErr") modalErr!: ElementRef;
  mensajeErr = "";

  constructor( private userService: UserService, private route: ActivatedRoute,
    private router: Router, 
    private tokenStorage: TokenStorageService,
    private modalService: NgbModal) {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
    this.mySubscription = this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.router.navigated = false;
      }
    });
  }

  ngOnInit() {
    this.getUsersComp();
  }

  getUsersComp() {
    this.userService.getUsers().subscribe(
      data => {
        var respStr = JSON.stringify(data);
        const resp = JSON.parse(respStr);
        this.users = resp._embedded.userList;
      },
      err => {
        this.content = err;
        this.mensajeErr = err.error.errors[0].message;
        this.modalService.open(this.modalErr);
      }
    );
  }

  closeModal(){
    this.modalService.dismissAll();
  }

  ngOnDestroy() {
    if (this.mySubscription) {
      this.mySubscription.unsubscribe();
    }
  }
}
