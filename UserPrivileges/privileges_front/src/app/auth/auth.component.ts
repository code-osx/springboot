import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from './auth.service';
import { TokenStorageService } from './token-storage.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  authForm!: FormGroup;
  messageError = "";

  constructor(private route: ActivatedRoute, private authService: AuthService, private tokenStorage: TokenStorageService, private router: Router) { }

  ngOnInit(): void {
    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true;
      this.router.navigate(['/roles'], { relativeTo: this.route });
    }
    this.initForm();
  }

  onSubmit(): void {
    this.authService.login(this.authForm.value['txtName'], this.authForm.value['txtPassword']).subscribe(
      data => {
        this.tokenStorage.saveToken(data.token);

        this.isLoginFailed = false;
        this.isLoggedIn = true;
        this.reloadPage();
      },
      err => {
        this.isLoginFailed = true;
        this.messageError = err.error.errors[0].message;
      }
    );
  }

  reloadPage(): void {
    window.location.reload();
  }

  private initForm() {
    let txtName = '';
    let txtPassword = '';

    this.authForm = new FormGroup({
      txtName: new FormControl(txtName, Validators.required),
      txtPassword: new FormControl(txtPassword, Validators.required)
    });
  }
}