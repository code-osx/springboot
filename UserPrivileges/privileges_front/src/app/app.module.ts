import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { AuthComponent } from './auth/auth.component';
import { RolesComponent } from './roles/roles.component';
import { UsersComponent } from './users/users.component';
import { UserService } from './users/users.service';
import { RolesListComponent } from './roles/roles-list/roles-list.component';
import { RoleNewEditComponent } from './roles/role-new-edit/role-new-edit.component';
import { RolesService } from './roles/roles.service';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    AuthComponent,
    RolesComponent,
    UsersComponent,
    RolesListComponent,
    RoleNewEditComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    UserService,
    RolesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
