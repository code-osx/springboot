package mx.santander.auth.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import mx.santander.auth.entity.User;

public interface UserRepository extends JpaRepository<User,Integer> {
    User findByUserName(String username);
}
