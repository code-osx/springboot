insert into privilege (id, name) values (1, 'read');
insert into privilege (id, name) values (2, 'edit');
insert into privilege (id, name) values (3, 'create');

insert into role (name, status, id_user_record, date_record, id_user_update, date_update) values ('Administrador', 1, 1, CURRENT_TIMESTAMP, 1, CURRENT_TIMESTAMP);
insert into role (name, status, id_user_record, date_record, id_user_update, date_update) values ('Usuario', 1, 2, CURRENT_TIMESTAMP, 2, CURRENT_TIMESTAMP);

insert into USER_TBL (id, user_name, password, email) values (1, 'oscar', 'pass123', 'osanchez@minsait.com');
insert into USER_TBL (id, user_name, password, email) values (2, 'alejandra', 'pass123', 'alejandra@correo.com');

insert into role_privilege (role_id, privilege_id) values (1, 1);
insert into role_privilege (role_id, privilege_id) values (1, 2);
insert into role_privilege (role_id, privilege_id) values (1, 3);
insert into role_privilege (role_id, privilege_id) values (2, 1);


insert into user_role (user_id, role_id) values (1, 1);
insert into user_role (user_id, role_id) values (1, 2);
insert into user_role (user_id, role_id) values (2, 2);
