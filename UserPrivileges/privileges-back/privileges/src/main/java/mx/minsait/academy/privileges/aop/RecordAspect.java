package mx.minsait.academy.privileges.aop;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import mx.minsait.academy.privileges.config.security.JwtUtil;
import mx.minsait.academy.privileges.entity.Record;
import mx.minsait.academy.privileges.service.impl.RecordService;

/**
* Minsait
* Component: RecordAspect.java
* Descripción: Componente utilizado para realizar la bitacora de peticiones realizadas.
*/
@Aspect
@Component
public class RecordAspect {
	@Autowired
	JwtUtil jwtUtil;

	@Autowired
	RecordService recordService;
	
	@Around("execution(* mx.minsait.academy.privileges.controller..*(..))")
	public Object profileAllControllers(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
		// ProceedingJoinPoint nos proporciona el nombre de la clase y el nombre del método que se va a ejecutar.
		MethodSignature methodSignature = (MethodSignature) proceedingJoinPoint.getSignature();

		// Get intercepted method details
		String className = methodSignature.getDeclaringType().getSimpleName();
		String controllerName = methodSignature.getName();

		// StopWatch, clase para capturar la hora de inicio del método y la hora de finalización del método.
		final StopWatch stopWatch = new StopWatch();

		// Measure method execution time
		stopWatch.start();
		Object result = proceedingJoinPoint.proceed();
		
		if(!"generateToken".equals(controllerName)) {
			RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
			if (requestAttributes != null) {
				HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();
				String fullToken = request.getHeader("Authorization");
				String token = fullToken.substring(7);
				long idUser = jwtUtil.extractIdUser(token);
				
				Record rec = new Record(null, idUser, className+"."+controllerName, new Date(), stopWatch.getTotalTimeMillis());
				recordService.createRecord(rec);
			  }
		}
		
		return result;
	}
}
