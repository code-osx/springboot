package mx.minsait.academy.privileges.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.MissingServletRequestParameterException;

import lombok.extern.slf4j.Slf4j;
import mx.minsait.academy.privileges.config.security.JwtUtil;
import mx.minsait.academy.privileges.controller.RoleManagementController;
import mx.minsait.academy.privileges.entity.Privilege;
import mx.minsait.academy.privileges.entity.Role;
import mx.minsait.academy.privileges.model.PrivilegeServiceGenericException;
import mx.minsait.academy.privileges.model.ResourceNotFoundException;
import mx.minsait.academy.privileges.repository.RoleRepository;
import mx.minsait.academy.privileges.service.IRoleManagementService;

/**
* Minsait
* Servicio: RoleManagementService.java
* Descripción: Servicio que se encarga de comunicarse con el repositorio y persistir la información de un Rol.
*/

@Service
@Slf4j
public class RoleManagementService implements IRoleManagementService {

	private RoleRepository roleRepository;
	private String linkToRoles = "roles";
	
	@Autowired
	JwtUtil jwtUtil;

	RoleManagementService(RoleRepository roleRepository){
		this.roleRepository = roleRepository;
	}
	
	@Override
	public CollectionModel<EntityModel<Role>> getRoles() throws PrivilegeServiceGenericException {
		log.info("Service: Se realiza la consulta al repostirio para obtener todos los roles con estatus true");
		List<EntityModel<Role>> roles = roleRepository.findByStatusTrue().stream().map(role -> {
						try {
							log.info("Service: Try - Se inserta el link con SelfRel para cada Rol");
							return EntityModel.of(role,
									linkTo(methodOn(RoleManagementController.class).getRole(role.getId())).withSelfRel());
						} catch (ResourceNotFoundException|PrivilegeServiceGenericException e) {
							throw new ResourceNotFoundException(e.getMessage());
						}
				}).collect(Collectors.toList());
		
		log.info("Service: Se realiza el retorno del resultado de la consulta al repositorio");
		return CollectionModel.of(roles, linkTo(methodOn(RoleManagementController.class).getRoles()).withSelfRel());
	}

	@Override
	public EntityModel<Optional<Role>> getRole(long id) throws ResourceNotFoundException, PrivilegeServiceGenericException {
		log.info("Service: Se realiza la consulta al repostirio para obtener un rol en especifico");
		Optional<Role> role = roleRepository.findById(id);
		
		if(role.isPresent()) {
			log.info("Service: Se encontró el rol");
			return EntityModel.of(role, 
					linkTo(methodOn(RoleManagementController.class).getRole(id)).withSelfRel(),
					linkTo(methodOn(RoleManagementController.class).getRoles()).withRel(linkToRoles));
		} else {
			log.info("Service: No se encontró el rol");
			throw new ResourceNotFoundException("No se encontro el rol");
		}
	}

	
	@Override
	public EntityModel<Role> createRole(Role role, String fullToken) throws MissingServletRequestParameterException, PrivilegeServiceGenericException {
		String token = fullToken.substring(7);
		long idUser = jwtUtil.extractIdUser(token);
		
		if(role.getName() != null && !"".equals(role.getName())) {
			if(role.getPrivileges().isEmpty()) {
				log.info("Service: El rol no contienen ningun privilegio, se agregará el privilegio read por defecto");
				List<Privilege> privileges = new ArrayList<>();
				privileges.add(new Privilege(1L, "read", null));
				role.setPrivileges(privileges);
			}
			
			role.setIdUserRecord(idUser);
			role.setStatus(true);
			role.setDateRecord(new Date());
			
			log.info("Service: El rol se almacena");
			Role savedRole = roleRepository.save(role);
			
			return EntityModel.of(
					savedRole, 
					linkTo(methodOn(RoleManagementController.class).getRole(savedRole.getId())).withSelfRel(),
					linkTo(methodOn(RoleManagementController.class).getRoles()).withRel(linkToRoles));
		} else {
			log.info("Service: El rol no tiene el parametro name");
			throw new MissingServletRequestParameterException("name", "name");
		}
	}

	
	@Override
	public EntityModel<Role> deleteRole(long id, String fullToken) throws ResourceNotFoundException, PrivilegeServiceGenericException {
		String token = fullToken.substring(7);
		long idUser = jwtUtil.extractIdUser(token);
		
		Optional<Role> role = roleRepository.findById(id);
		
		if(!role.isEmpty()) {
			log.info("Service: Se encontró el rol a eliminar");
			Role roleDeleted = role.get();
			roleDeleted.setStatus(false);
			roleDeleted.setIdUserUpdate(idUser);
			roleDeleted.setDateUpdate(new Date());
			
			Role savedRole = roleRepository.save(roleDeleted);
			
			return EntityModel.of(
					savedRole, 
					linkTo(methodOn(RoleManagementController.class).getRole(savedRole.getId())).withSelfRel(),
					linkTo(methodOn(RoleManagementController.class).getRoles()).withRel(linkToRoles));
		} else {
			log.info("Service: No se encontró el rol a eliminar");
			throw new ResourceNotFoundException("No se encontro el rol a eliminar");
		}
	}

	
	@Override
	public EntityModel<Role> updateRole(long id, Role roleBody, String fullToken) throws ResourceNotFoundException, MissingServletRequestParameterException, PrivilegeServiceGenericException {
		String token = fullToken.substring(7);
		long idUser = jwtUtil.extractIdUser(token);
		
		Optional<Role> role = roleRepository.findById(id);
		
		if(!role.isEmpty()) {
			log.info("Service: Se encontró el rol a actualizar");
			if(roleBody.getName() != null && !"".equals(roleBody.getName())) {
				if(roleBody.getPrivileges().isEmpty()) {
					log.info("Service: El rol no contienen ningun privilegio, se agregará el privilegio read por defecto");
					List<Privilege> privileges = new ArrayList<>();
					privileges.add(new Privilege(1L, "read", null));
					roleBody.setPrivileges(privileges);
				}
				
				Role roleUpdate = role.get();
				roleUpdate.setIdUserUpdate(idUser);
				roleUpdate.setDateUpdate(new Date());
				roleUpdate.setName(roleBody.getName());
				roleUpdate.setPrivileges(roleBody.getPrivileges());
				
				Role savedRole = roleRepository.save(roleUpdate);
				log.info("Service: El rol es actualizado");
				return EntityModel.of(
						savedRole, 
						linkTo(methodOn(RoleManagementController.class).getRole(savedRole.getId())).withSelfRel(),
						linkTo(methodOn(RoleManagementController.class).getRoles()).withRel(linkToRoles));
			} else {
				log.info("Service: El rol no tiene el parametro name");
				throw new MissingServletRequestParameterException("name", "name");
			}
			
		} else {
			log.info("Service: No se encontró el rol a actualizar");
			throw new ResourceNotFoundException("No se encontro el rol a actualizar");
		}
	}
	
}
