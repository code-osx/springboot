package mx.minsait.academy.privileges.util;

/**
 * @author Minsait
 * 
 * Clase de constantes para el microservicio
 */
public final class CollectionsConstantes {
	
	public static final String ERROR = "Error";

	public static final String WARNING = "Warning";
	
	
	
	private CollectionsConstantes() {} // previene instanciacion
	
}