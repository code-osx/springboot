package mx.minsait.academy.privileges.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import mx.minsait.academy.privileges.entity.Role;

/**
* Minsait
* Repositorio: RoleRepository.java
* Descripción: Interfaz utilizada para persistir la información de un Rol.
*/

@Repository
public interface RoleRepository extends JpaRepository<Role, Long>{
	List<Role> findByStatusTrue();
}
