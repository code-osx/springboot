package mx.minsait.academy.privileges.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
* Minsait
* Entidad: Record.java
* Descripción: Entidad utilizada para persistir la informacion de la bitácora.
* Uso de lombok para no especificar setters, getters y constructor
*/
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "Modelo Privilege para base de datos")
public class Record {
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private Long id;
	private Long idUser;
	private String method;
	private Date dateExc;
	private Long execTime;
}
