package mx.minsait.academy.privileges.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import mx.minsait.academy.privileges.entity.Record;

/**
* Minsait
* Repositorio: RecordRepository.java
* Descripción: Interfaz utilizada para persistir la información de la bitácora.
*/

@Repository
public interface RecordRepository extends JpaRepository<Record, Long>{

}
