package mx.minsait.academy.privileges.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.minsait.academy.privileges.entity.Record;
import mx.minsait.academy.privileges.model.PrivilegeServiceGenericException;
import mx.minsait.academy.privileges.model.ResourceNotFoundException;
import mx.minsait.academy.privileges.repository.RecordRepository;

@Service
public class RecordService {
	@Autowired
    private RecordRepository repository;
	
	
	/**
	* Método encargado de guardar el registro en la bitácora
	* @param id
	* @return EntityModel<Optional<User>>
	* @throws ResourceNotFoundException
	* @throws PrivilegeServiceGenericException 
	*/
	public Record createRecord(Record recordObj) {
		return repository.save(recordObj);
	}
}
