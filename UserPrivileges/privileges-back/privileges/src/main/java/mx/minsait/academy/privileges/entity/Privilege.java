package mx.minsait.academy.privileges.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
* Minsait
* Entidad: Privilege.java
* Descripción: Entidad utilizada para persistir la informacion de un Privilegio.
* Uso de lombok para no especificar setters, getters y constructor
*/

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "Modelo Privilege para base de datos")
public class Privilege {
	@Id
	private Long id;
	private String name;
	
	@JsonIgnore
	@ManyToMany(mappedBy = "privileges")
	private List<Role> roles = new ArrayList<>();
	
}
