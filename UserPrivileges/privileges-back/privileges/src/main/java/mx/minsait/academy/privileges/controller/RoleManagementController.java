package mx.minsait.academy.privileges.controller;

import java.util.Optional;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import mx.minsait.academy.privileges.entity.Role;
import mx.minsait.academy.privileges.model.PrivilegeServiceGenericException;
import mx.minsait.academy.privileges.model.RoleModel;
import mx.minsait.academy.privileges.model.ResourceNotFoundException;
import mx.minsait.academy.privileges.service.IRoleManagementService;

/**
 * Minsait Controlador: RoleManagementController.java Descripción: Controlador
 * encargado de recibir las peticiones REST e invocar el servicio
 * correspondiente.
 */

@RestController
@RequestMapping("/roles")
@Api(value = "Crear, Leer, Actualizar y Borrar Roles", consumes = "application/json")
@Slf4j
public class RoleManagementController {

	private IRoleManagementService roleManagService;

	RoleManagementController(IRoleManagementService roleManagService) {
		this.roleManagService = roleManagService;
	}

	/**
	 * Método encargado de invocar el servicio que proporciona todos los roles
	 * @url: /roles
	 * @return ResponseEntity<CollectionModel<EntityModel<Role>>>
	 * @throws PrivilegeServiceGenericException 
	 */
	@ApiOperation(value = "Obtener todos los roles.", produces = "application/json", response = Role.class)
	@ApiResponses(value = {
		@ApiResponse(code = 200, message = "Ok"),
		@ApiResponse(code = 401, message = "Unauthorized"), 
		@ApiResponse(code = 403, message = "Forbidden"),
		@ApiResponse(code = 500, message = "Internal Server Error")
	})
	@GetMapping
	public ResponseEntity<CollectionModel<EntityModel<Role>>> getRoles() throws PrivilegeServiceGenericException {
		log.info("Controller: Ejecion de servicio getRoles()");
		return new ResponseEntity<>(roleManagService.getRoles(), HttpStatus.OK);
	}

	/**
	 * Método encargado de invocar el servicio que se encarga de obtener un rol
	 * especifico
	 * @url: /roles/{id}
	 * @param @PathVariable long id
	 * @return ResponseEntity<EntityModel<Optional<Role>>>
	 * @throws ResourceNotFoundException 
	 * @throws PrivilegeServiceGenericException 
	 */
	@ApiOperation(value = "Obtener un rol tomando como referencia su id.", produces = "application/json", response = Role.class)
	@ApiResponses(value = {
		@ApiResponse(code = 200, message = "Ok"),
		@ApiResponse(code = 401, message = "Unauthorized"),
		@ApiResponse(code = 403, message = "Forbidden"),
		@ApiResponse(code = 404, message = "Not Found"),
		@ApiResponse(code = 500, message = "Internal Server Error")
	})
	@GetMapping("/{id}")
	public ResponseEntity<EntityModel<Optional<Role>>> getRole(@PathVariable long id) throws ResourceNotFoundException, PrivilegeServiceGenericException {
		log.info("Controller: Ejecion de servicio getRole({id})");
		return new ResponseEntity<>(roleManagService.getRole(id), HttpStatus.OK);
	}

	/**
	 * Método encargado de invocar el servicio que se encarga de registrar un nuevo
	 * rol en la base de datos
	 * @url: /role
	 * @param: @RequestBody RoleModel roleModel, @RequestHeader("Authorization") String fullToken
	 * @throws MissingServletRequestParameterException 
	 * @throws PrivilegeServiceGenericException 
	 */
	@ApiOperation(value = "Crear un nuevo rol.")
	@ApiResponses(value = {
		@ApiResponse(code = 201, message = "Created"),
		@ApiResponse(code = 401, message = "Unauthorized"),
		@ApiResponse(code = 403, message = "Forbidden"),
		@ApiResponse(code = 500, message = "Internal Server Error")
	})
	@PostMapping("")
	public ResponseEntity<EntityModel<Role>> createRole(@RequestBody RoleModel roleModel, @RequestHeader("Authorization") String fullToken) throws MissingServletRequestParameterException, PrivilegeServiceGenericException {
		Role role = new Role(roleModel);
		log.info("Controller: Ejecion de servicio createRole({id})");
		return new ResponseEntity<>(roleManagService.createRole(role, fullToken), HttpStatus.CREATED);
	}

	/**
	 * Método encargado de invocar el servicio que se encarga de eliminar un rol
	 * especifico (cambiar su estatus)
	 * @url: /role/{id}
	 * @param @PathVariable long id, @RequestHeader("Authorization") String fullToken
	 * @return ResponseEntity<EntityModel<Role>>
	 * @throws ResourceNotFoundException 
	 * @throws PrivilegeServiceGenericException 
	 */
	@ApiOperation(value = "Eliminar un rol tomando en cuenta su referencia id.")
	@ApiResponses(value = {
		@ApiResponse(code = 200, message = "Ok"),
		@ApiResponse(code = 401, message = "Unauthorized"),
		@ApiResponse(code = 403, message = "Forbidden"),
		@ApiResponse(code = 404, message = "Not Found"),
		@ApiResponse(code = 500, message = "Internal Server Error")
	})
	@DeleteMapping("/{id}")
	public ResponseEntity<EntityModel<Role>> deleteRole(@PathVariable long id, @RequestHeader("Authorization") String fullToken) throws ResourceNotFoundException, PrivilegeServiceGenericException {
		log.info("Controller: Ejecion de servicio deleteRole()");
		return new ResponseEntity<>(roleManagService.deleteRole(id, fullToken), HttpStatus.OK);
	}

	/**
	 * Método encargado de invocar el servicio que se encarga de actualizar un rol especifico
	 * @url: /role/{id}
	 * @param @PathVariable long id, @RequestBody RoleModel roleModel, @RequestHeader("Authorization")
	 * @return ResponseEntity<EntityModel<Role>>
	 * @throws PrivilegeServiceGenericException 
	 * @throws ResourceNotFoundException 
	 * @throws MissingServletRequestParameterException 
	 */
	@ApiOperation(value = "Actualizar la informacion de un rol existente tomando en cuenta su referencia id.")
	@ApiResponses(value = {
		@ApiResponse(code = 200, message = "Ok"),
		@ApiResponse(code = 401, message = "Unauthorized"),
		@ApiResponse(code = 403, message = "Forbidden"),
		@ApiResponse(code = 404, message = "Not Found"),
		@ApiResponse(code = 500, message = "Internal Server Error")
	})
	@PutMapping("/{id}")
	public ResponseEntity<EntityModel<Role>> updateRole(@PathVariable long id, @RequestBody RoleModel roleModel, @RequestHeader("Authorization") String fullToken) throws PrivilegeServiceGenericException, MissingServletRequestParameterException, ResourceNotFoundException {
		Role role = new Role(roleModel);
		log.info("Controller: Ejecion de servicio updateRole()");
		return new ResponseEntity<>(roleManagService.updateRole(id, role, fullToken), HttpStatus.OK);
	}
}
