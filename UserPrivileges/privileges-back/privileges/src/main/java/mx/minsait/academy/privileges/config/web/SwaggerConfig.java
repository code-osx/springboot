package mx.minsait.academy.privileges.config.web;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
* Minsait
* Configuracion: SwaggerConfig.java
* Descripción: Configuración Swagger.
*/
@Configuration
@EnableSwagger2
public class SwaggerConfig {
	/**
	 * Api docket.
	 *
	 * @return the docket
	 */
         
	@Bean
	public Docket api() { 
		return new Docket(DocumentationType.SWAGGER_2)  
				.select()                                  
				.apis(RequestHandlerSelectors.basePackage("mx.minsait.academy.privileges"))
				.paths(PathSelectors.any())
				.build()
				.apiInfo(apiInfo());
	}

	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("swagger-ui.html")
		.addResourceLocations("classpath:/META-INF/resources/");

		registry.addResourceHandler("/webjars/**")
		.addResourceLocations("classpath:/META-INF/resources/webjars/");
	}
	
	private ApiInfo apiInfo() {
	    return new ApiInfoBuilder().title("Role Management").version("1.0")
	            .description("Configuración de privilegios. \nPermite listar los roles existentes, crear, modificar y eliminar un rol. \nUn rol puede contener distintos privilegios, los cuales pueden ser de lectura, edición y creación.\nSi hay algún rol que debe ser eliminado o modificado primero se debe corroborar si hay usuarios relacionados al rol y se debe de generar un mensaje de advertencia que deberá de ser confirmado para realizar la acción.")
	            .build();
	}
}

