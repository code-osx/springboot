package mx.minsait.academy.privileges.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import mx.minsait.academy.privileges.entity.User;

/**
* Minsait
* Repositorio: UserRepository.java
* Descripción: Interfaz utilizada para validar el token que obtenemos del header "Authorization".
* También persiste la información de User
*/

public interface UserRepository extends JpaRepository<User,Long> {
    User findByUserName(String username);
    
    @Query("SELECT count(u.id) FROM User u JOIN u.roles r WHERE r.id = :param")
    public int usuariosRelacionados(@Param("param") Long param);
    
}
