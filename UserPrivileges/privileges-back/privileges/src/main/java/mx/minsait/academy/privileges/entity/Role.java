package mx.minsait.academy.privileges.entity;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import mx.minsait.academy.privileges.model.RoleModel;

/**
* Minsait
* Entidad: Role.java
* Descripción: Entidad utilizada para persistir la informacion de un Rol.
* Uso de lombok para no especificar setters, getters y constructor
*/

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "Modelo Role para base de datos")
public class Role {
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private Long id;
	private String name;
	@JsonIgnore
	private Boolean status;
	@JsonIgnore
	private Long idUserRecord;
	@JsonIgnore
	private Date dateRecord;
	@JsonIgnore
	private Long idUserUpdate;
	@JsonIgnore
	private Date dateUpdate;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "role_privilege",
		joinColumns = {@JoinColumn(name="role_id")},
		inverseJoinColumns = {@JoinColumn(name="privilege_id")}
	)
	private List<Privilege> privileges = new ArrayList<>();
	
	@JsonIgnore
	@ManyToMany(mappedBy = "roles")
	private List<User> users = new ArrayList<>();
	
	public Role(RoleModel roleModel){
		this.id = roleModel.getId();
		this.name = roleModel.getName();
		this.privileges = roleModel.getPrivileges();
	}
}
