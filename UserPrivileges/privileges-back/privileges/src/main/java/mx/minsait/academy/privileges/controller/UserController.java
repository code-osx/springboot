package mx.minsait.academy.privileges.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import mx.minsait.academy.privileges.config.security.JwtUtil;
import mx.minsait.academy.privileges.entity.User;
import mx.minsait.academy.privileges.model.PrivilegeServiceGenericException;
import mx.minsait.academy.privileges.model.ResourceNotFoundException;
import mx.minsait.academy.privileges.service.impl.UserService;
import net.minidev.json.JSONObject;

/**
 * Minsait Controlador: UserController.java 
 * Descripción: Controlador encargado de recibir las peticiones REST e invocar el servicio
 * correspondiente.
 */
@RestController
@RequestMapping("/users")
@Api(value = "Obtener informacion sobre los usuarios y sus roles", consumes = "application/json")
@Slf4j
public class UserController {
	
	private UserService userService;
	User userRes;

	@Autowired
	JwtUtil jwtUtil;

	UserController(UserService userService) {
		this.userService = userService;
	}
	
	/**
	 * Método encargado de invocar el servicio que se encarga de obtener un usuario especifico
	 * @url: /users/{id}
	 * @param: long id
	 * @return ResponseEntity<EntityModel<Optional<User>>>
	 * @throws ResourceNotFoundException
	 * @throws PrivilegeServiceGenericException 
	 */
	@ApiOperation(value = "Obtener un usuario y sus roles tomando como referencia su id.", produces = "application/json", response = User.class)
	@ApiResponses(value = {
		@ApiResponse(code = 200, message = "Ok"),
		@ApiResponse(code = 401, message = "Unauthorized"),
		@ApiResponse(code = 403, message = "Forbidden"),
		@ApiResponse(code = 404, message = "Not Found"),
		@ApiResponse(code = 500, message = "Internal Server Error")
	})
	@GetMapping("/{id}/roles")
	public ResponseEntity<EntityModel<Optional<User>>> getUser(@PathVariable long id) throws ResourceNotFoundException, PrivilegeServiceGenericException {
		log.info("Controller: Ejecucion de servicio getUser({id})");
		return new ResponseEntity<>(userService.getUser(id), HttpStatus.OK);
	}
	
	/**
	 * Método encargado de invocar el servicio que proporciona todos los usuarios y sus roles
	 * @url:/users
	 * @return ResponseEntity<CollectionModel<EntityModel<User>>>
	 * @throws PrivilegeServiceGenericException 
	 */
	@ApiOperation(value = "Obtener todos los usuarios y sus respectivos roles.", produces = "application/json", response = User.class)
	@ApiResponses(value = {
		@ApiResponse(code = 200, message = "Ok"),
		@ApiResponse(code = 401, message = "Unauthorized"), 
		@ApiResponse(code = 403, message = "Forbidden"),
		@ApiResponse(code = 500, message = "Internal Server Error")
	})
	@GetMapping("/roles")
	public ResponseEntity<CollectionModel<EntityModel<User>>> getUsers() throws PrivilegeServiceGenericException {
		log.info("Controller: Ejecion de servicio getUsers()");
		return new ResponseEntity<>(userService.getUsers(), HttpStatus.OK);
	}
	
	/**
	 * Método encargado de corroborar si hay usuarios relacionados con un rol
	 * @url:/role/{id}
	 * @return ResponseEntity<Object>
	 */
	@ApiOperation(value = "Valida si hay usuarios relacionados con un rol", produces = "application/json")
	@ApiResponses(value = {
		@ApiResponse(code = 200, message = "Ok"),
		@ApiResponse(code = 401, message = "Unauthorized"), 
		@ApiResponse(code = 403, message = "Forbidden"),
		@ApiResponse(code = 500, message = "Internal Server Error")
	})
	@GetMapping("/role/{id}")
    public ResponseEntity<Object> getUsersRelacionados(@PathVariable long id){
        boolean resp = userService.getUsersRelacionados(id);
        
        JSONObject entity = new JSONObject();
        entity.put("usersRel", resp);
        return new ResponseEntity<>(entity, HttpStatus.OK);
    }
}
