package mx.minsait.academy.privileges.model;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import mx.minsait.academy.privileges.entity.Privilege;

@Data
public class RoleModel {
	private Long id;
	private String name;
	private List<Privilege> privileges = new ArrayList<>();
}
