package mx.minsait.academy.privileges.service;

import java.util.Optional;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.web.bind.MissingServletRequestParameterException;

import mx.minsait.academy.privileges.entity.Role;
import mx.minsait.academy.privileges.model.PrivilegeServiceGenericException;
import mx.minsait.academy.privileges.model.ResourceNotFoundException;

/**
* Minsait
* Interfaz: IRoleManagementService.java
* Descripción: Interfaz que define a RoleManagementService.java
*/

public interface IRoleManagementService {
	/**
	* Método encargado de obtener todos los roles existentes
	* @param
	* @return CollectionModel<EntityModel<Role>>
	* @throws PrivilegeServiceGenericException 
	*/
	public CollectionModel<EntityModel<Role>> getRoles() throws PrivilegeServiceGenericException;
	
	/**
	* Método encargado de obtener uno de los roles de la base de datos
	* @param id
	* @return EntityModel<Optional<Role>>
	* @throws ResourceNotFoundException
	* @throws PrivilegeServiceGenericException 
	*/
	public EntityModel<Optional<Role>> getRole(long id) throws ResourceNotFoundException, PrivilegeServiceGenericException;
	
	/**
	* Método encargado registrar un nuevo rol en la base de datos
	* @param Role role, String fullToken
	* @return EntityModel<Role>
	* @throws MissingServletRequestParameterException
	* @throws PrivilegeServiceGenericException 
	*/
	public EntityModel<Role> createRole(Role role, String fullToken) throws MissingServletRequestParameterException, PrivilegeServiceGenericException;
	
	/**
	* Método encargado actualizar el campo "status" de un rol existente en la base de datos colocando el valor de false
	* @param long id, String fullToken
	* @return EntityModel<Role>
	* @throws ResourceNotFoundException 
	* @throws PrivilegeServiceGenericException 
	*/
	public EntityModel<Role> deleteRole(long id, String fullToken) throws ResourceNotFoundException, PrivilegeServiceGenericException;
	
	/**
	* Método encargado actilizar un rol existente de la base de datos
	* @param long id, Role role, String fullToken
	* @return EntityModel<Role>
	* @throws ResourceNotFoundException 
	* @throws MissingServletRequestParameterException 
	* @throws PrivilegeServiceGenericException 
	*/
	public EntityModel<Role> updateRole(long id, Role role, String fullToken) throws  ResourceNotFoundException, MissingServletRequestParameterException, PrivilegeServiceGenericException;

}
