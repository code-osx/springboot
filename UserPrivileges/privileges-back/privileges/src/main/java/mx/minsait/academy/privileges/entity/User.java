package mx.minsait.academy.privileges.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
* Minsait
* Entidad: User.java
* Descripción: Entidad utilizada para validar el token que obtenemos del header "Authorization".
* Entidad utilizada para persistir la informacion de un Usuario. 
* Uso de lombok para no especificar setters, getters y constructor.
*/

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "Modelo Usuario para base de datos")
@Table(name="USER_TBL")
public class User {
    @Id
    private long id;
    private String userName;
    @JsonIgnore
    private String password;
    private String email;
    
    @ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "user_role",
		joinColumns = {@JoinColumn(name="user_id")},
		inverseJoinColumns = {@JoinColumn(name="role_id")}
	)
	private List<Role> roles = new ArrayList<>();
}

