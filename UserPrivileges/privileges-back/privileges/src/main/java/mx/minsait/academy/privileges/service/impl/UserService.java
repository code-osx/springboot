package mx.minsait.academy.privileges.service.impl;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import mx.minsait.academy.privileges.controller.UserController;
import mx.minsait.academy.privileges.entity.User;
import mx.minsait.academy.privileges.model.PrivilegeServiceGenericException;
import mx.minsait.academy.privileges.model.ResourceNotFoundException;
import mx.minsait.academy.privileges.repository.UserRepository;

/**
* Minsait
* Servicio: UserService.java
* Descripción: Servicio que se encarga de comunicarse con el repositorio y validar el token recibido en el header "Authorization".
* También es utilizado para obtener todos los usuarios y un usuario en especifico con su respectivo rol.
*/

@Service
@Slf4j
public class UserService implements UserDetailsService {
    @Autowired
    private UserRepository repository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = repository.findByUserName(username);
        return new org.springframework.security.core.userdetails.User(user.getUserName(), user.getPassword(), new ArrayList<>());
    }
	
	/**
	 * 
	 */
    /**
	* Método encargado de obtener uno de los usuarios de la base de datos
	* @param id
	* @return EntityModel<Optional<User>>
	* @throws ResourceNotFoundException
	* @throws PrivilegeServiceGenericException 
	*/
	public EntityModel<Optional<User>> getUser(long id) throws ResourceNotFoundException, PrivilegeServiceGenericException {
		log.info("Service: Se realiza la consulta al repostirio para obtener un usuario en especifico");
		Optional<User> user = repository.findById(id);
		
		if(!user.isEmpty()) {
			log.info("Service: Se encontró el usuario");
			return EntityModel.of(user, 
					linkTo(methodOn(UserController.class).getUser(id)).withSelfRel(),
					linkTo(methodOn(UserController.class).getUsers()).withRel("users"));
			
		} else {
			log.info("Service: No se encontró el usuario");
			throw new ResourceNotFoundException("No se encontro el usuario");
		}
	}
	
	/**
	* Método encargado de obtener todos los usuarios de la base de datos
	* @param
	* @return CollectionModel<EntityModel<User>>
	* @throws PrivilegeServiceGenericException
	*/
	public CollectionModel<EntityModel<User>> getUsers() throws PrivilegeServiceGenericException {
		log.info("Service: Se realiza la consulta al repostirio para obtener todos los roles con estatus true");
		List<EntityModel<User>> users = repository.findAll().stream().map(user -> {
						try {
							log.info("Service: Try - Se inserta el link con SelfRel para cada Usuario");
							return EntityModel.of(user,
									linkTo(methodOn(UserController.class).getUser(user.getId())).withSelfRel());
						} catch (ResourceNotFoundException|PrivilegeServiceGenericException e) {
							throw new ResourceNotFoundException(e.getMessage());
						}
				}).collect(Collectors.toList());
		
		log.info("Service: Se realiza el retorno del resultado de la consulta al repositorio");
		return CollectionModel.of(users, linkTo(methodOn(UserController.class).getUsers()).withSelfRel());
	}
	
	/**
	* Método encargado de verificar si existe relacion de un rol con Usuarios
	* @param long id, String fullToken
	* @return boolean
	*/
	public boolean getUsersRelacionados(long id) {
		int usuarios = repository.usuariosRelacionados(id);
		
		return (usuarios > 0);
	}
}
