package mx.minsait.academy.privileges.entity;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

class PrivilegeTest {
	@Test
	void create() throws ParseException {
		List<Role> arrayListRole = new ArrayList<>();
		
		Privilege privilege = new Privilege();
		privilege.setId(1L);
		privilege.setName("Admin");
		privilege.setRoles(arrayListRole);
		
		
		assertEquals(1L, privilege.getId(), "Se esperaba equivalencia");
		assertEquals("Admin", privilege.getName(), "Se esperaba equivalencia");
		assertEquals(arrayListRole, privilege.getRoles(), "Se esperaba equivalencia");
	}
}
