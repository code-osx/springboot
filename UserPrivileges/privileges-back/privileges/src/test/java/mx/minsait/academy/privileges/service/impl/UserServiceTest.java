package mx.minsait.academy.privileges.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;

import mx.minsait.academy.privileges.config.security.JwtUtil;
import mx.minsait.academy.privileges.entity.User;
import mx.minsait.academy.privileges.model.PrivilegeServiceGenericException;
import mx.minsait.academy.privileges.model.ResourceNotFoundException;
import mx.minsait.academy.privileges.repository.UserRepository;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {
	@Mock
	private UserRepository userRepository;
	
	@Mock
	private JwtUtil jwtUtil = mock(JwtUtil.class);
	
	@InjectMocks
    private UserService userService;
	
	List<User> usersEsperados = new ArrayList<User>();
	String token;
	
	@BeforeEach
	void setUp() {
		MockitoAnnotations.initMocks(this);
		
		usersEsperados.add(new User(1L, "Usuario1", "pass", "correo@correo.com", null));
		usersEsperados.add(new User(2L, "Usuario2", "pass", "correo@correo.com", null));
		
		token = "token-test";
	}
	
	@Test
	void getUsersTest() throws PrivilegeServiceGenericException {
		when(userRepository.findAll()).thenReturn(usersEsperados);
		
		CollectionModel<EntityModel<User>> usersReales = userService.getUsers();
		
		assertEquals(2, usersReales.getContent().size());
	}
	
	@Test
	void getUserTest() throws ResourceNotFoundException, PrivilegeServiceGenericException{
		Optional<User> userOp = Optional.of(usersEsperados.get(0));
		when(userRepository.findById(1L)).thenReturn(userOp);
		
		EntityModel<Optional<User>> userReal = userService.getUser(1L);
		
		assertEquals("Usuario1", userReal.getContent().get().getUserName());
	}
}
