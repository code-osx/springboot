package mx.minsait.academy.privileges.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import mx.minsait.academy.privileges.entity.Privilege;

class RoleModelTest {
	@Test
	void create() {
		List<Privilege> arrayListPrivilege = new ArrayList<>();
		
		RoleModel roleModel = new RoleModel();
		roleModel.setId(1L);
		roleModel.setName("Admin");
		roleModel.setPrivileges(arrayListPrivilege);
		
		
		assertEquals(1L, roleModel.getId(), "Se esperaba equivalencia");
		assertEquals("Admin", roleModel.getName(), "Se esperaba equivalencia");
		assertEquals(arrayListPrivilege, roleModel.getPrivileges(), "Se esperaba equivalencia");
	}
}
