package mx.minsait.academy.privileges.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import lombok.extern.slf4j.Slf4j;
import mx.minsait.academy.privileges.entity.User;
import mx.minsait.academy.privileges.model.PrivilegeServiceGenericException;
import mx.minsait.academy.privileges.service.impl.UserService;

@Slf4j
@ExtendWith(MockitoExtension.class)
class UserControllerTest {
	@Autowired
	private MockMvc mvc;
	
	List<User> users = new ArrayList<User>();
	String token;
	
	@Mock
	private UserService userService;
	
	@InjectMocks
    private UserController userCont;
	
	@BeforeEach
    public void setup(){
		token = "token-test";
		
		users.add(new User(1L, "Usuario1", "pass", "correo@correo.com", null));
		users.add(new User(2L, "Usuario2", "pass", "correo@correo.com", null));
		
    	mvc = MockMvcBuilders.standaloneSetup(userCont).build();
    }
	
	@Test
	void getUserTest() throws Exception {
		List<EntityModel<User>> usersEnt = users.stream().map(user -> EntityModel.of(user)).collect(Collectors.toList());
		CollectionModel<EntityModel<User>> userColl = CollectionModel.of(usersEnt);
		
		when(userService.getUsers()).thenReturn(userColl);
		
		mvc.perform(get("/users/roles/").contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk());
	}
	@Test
	void getUserExceptionTest() throws PrivilegeServiceGenericException {
		when(userService.getUsers()).thenThrow(PrivilegeServiceGenericException.class);
		
		try {
			mvc.perform(get("/users/roles/").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isInternalServerError());
		} catch (Exception e) {
			log.info(e.toString());
		}
	}
}
