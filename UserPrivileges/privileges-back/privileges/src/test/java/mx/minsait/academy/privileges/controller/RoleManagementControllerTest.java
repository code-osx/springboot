package mx.minsait.academy.privileges.controller;

import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.bind.MissingServletRequestParameterException;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;
import mx.minsait.academy.privileges.entity.Privilege;
import mx.minsait.academy.privileges.entity.Role;
import mx.minsait.academy.privileges.model.PrivilegeServiceGenericException;
import mx.minsait.academy.privileges.model.ResourceNotFoundException;
import mx.minsait.academy.privileges.service.impl.RoleManagementService;

@Slf4j
@ExtendWith(MockitoExtension.class)
class RoleManagementControllerTest {
	@Autowired
	private MockMvc mvc;
	
	List<Role> roles = new ArrayList<Role>();
	List<Privilege> privilegesAdmin = new ArrayList<Privilege>();
	List<Privilege> privilegesUser = new ArrayList<Privilege>();
	String token;
	
	@Mock
	private RoleManagementService roleManagService;
	
	@InjectMocks
    private RoleManagementController gestionRolesCont;
	
	@BeforeEach
    public void setup(){
		token = "token-test";
		
		roles.add(new Role(1L, "Admin", true, 1L, new Date(), 1L, new Date(), null, null));
		roles.add(new Role(2L, "User", true, 2L, new Date(), 2L, new Date(), null, null));
		
    	mvc = MockMvcBuilders.standaloneSetup(gestionRolesCont).build();
    }
	
	@Test
	void getRolesTest() throws Exception {
		List<EntityModel<Role>> rolesEnt = roles.stream().map(role -> EntityModel.of(role)).collect(Collectors.toList());
		CollectionModel<EntityModel<Role>> rolesColl = CollectionModel.of(rolesEnt);
		
		when(roleManagService.getRoles()).thenReturn(rolesColl);
		
		mvc.perform(get("/roles/").contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk());
	}
	@Test
	void getRolesExceptionTest() throws PrivilegeServiceGenericException {
		when(roleManagService.getRoles()).thenThrow(PrivilegeServiceGenericException.class);
		
		try {
			mvc.perform(get("/roles/").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isInternalServerError());
		} catch (Exception e) {
			log.info(e.toString());
		}
	}
	
	@Test
	void getRoleTest() throws Exception {
		Role role = roles.get(0);
		EntityModel<Optional<Role>> roleModel = EntityModel.of(Optional.of(role));
		
		when(roleManagService.getRole(1L)).thenReturn(roleModel);
		
		mvc.perform(get("/roles/1").contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk());
	}
	@Test
	void getRoleNotFoundTest() throws ResourceNotFoundException, PrivilegeServiceGenericException {
		when(roleManagService.getRole(1L)).thenThrow(ResourceNotFoundException.class);
		
		try {
			mvc.perform(get("/roles/1").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());
		} catch (Exception e) {
			log.info(e.toString());
		}
	}
	
	@Test
	void createRoleTest() throws Exception {
		final ObjectMapper objectMapper = new ObjectMapper();
		Role newRole = new Role(1L, "Prueba", true, 1L, new Date(), 1L, new Date(), null, null);
		EntityModel<Role> roleModel = EntityModel.of(newRole);
		
		lenient().when(roleManagService.createRole(newRole, "Bearer"+token)).thenReturn(roleModel);
		
		mvc.perform(post("/roles/").contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(newRole)).header("Authorization", "Bearer "+token))
		.andExpect(status().isCreated());
	}
	@Test
	void createRoleNotFoundTest() throws ResourceNotFoundException, PrivilegeServiceGenericException, MissingServletRequestParameterException {
		final ObjectMapper objectMapper = new ObjectMapper();
		Role newRole = new Role(1L, "Prueba", true, 1L, new Date(), 1L, new Date(), null, null);
		
		when(roleManagService.createRole(newRole, token)).thenThrow(ResourceNotFoundException.class);
		
		try {
			mvc.perform(post("/roles/").contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(newRole)).header("Authorization", "Bearer "+token))
			.andExpect(status().isNotFound());
		} catch (Exception e) {
			log.info(e.toString());
		}
	}
	@Test
	void createRoleMissingRequestTest() throws ResourceNotFoundException, PrivilegeServiceGenericException, MissingServletRequestParameterException {
		final ObjectMapper objectMapper = new ObjectMapper();
		Role newRole = new Role(1L, "Prueba", true, 1L, new Date(), 1L, new Date(), null, null);
		
		when(roleManagService.createRole(newRole, token)).thenThrow(MissingServletRequestParameterException.class);
		
		try {
			mvc.perform(post("/roles/").contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(newRole)).header("Authorization", "Bearer "+token))
			.andExpect(status().isBadRequest());
		} catch (Exception e) {
			log.info(e.toString());
		}
	}
	
	@Test
	void deleteRoleTest() throws Exception {
		Role newRole = new Role(1L, "Prueba", true, 1L, new Date(), 1L, new Date(), null, null);
		EntityModel<Role> roleModel = EntityModel.of(newRole);
		
		lenient().when(roleManagService.deleteRole(1L, token)).thenReturn(roleModel);
		
		mvc.perform(delete("/roles/1").contentType(MediaType.APPLICATION_JSON).header("Authorization", "Bearer "+token))
		.andExpect(status().isOk());
	}
	@Test
	void deleteRoleNotFoundTest() throws ResourceNotFoundException, PrivilegeServiceGenericException {
		when(roleManagService.deleteRole(1L, token)).thenThrow(ResourceNotFoundException.class);
		
		try {
			mvc.perform(delete("/roles/1").contentType(MediaType.APPLICATION_JSON).header("Authorization", "Bearer "+token)).andExpect(status().isNotFound());
		} catch (Exception e) {
			log.info(e.toString());
		}
	}
	
	@Test
	void updateRoleTest() throws Exception {
		final ObjectMapper objectMapper = new ObjectMapper();
		Role newRole = new Role(1L, "Prueba", true, 1L, new Date(), 1L, new Date(), null, null);
		EntityModel<Role> roleModel = EntityModel.of(newRole);
		
		lenient().when(roleManagService.updateRole(1L, newRole, token)).thenReturn(roleModel);
		
		mvc.perform(put("/roles/1").contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(newRole)).header("Authorization", "Bearer "+token))
		.andExpect(status().isOk());
	}
	@Test
	void updateRoleNotFoundTest() throws ResourceNotFoundException, PrivilegeServiceGenericException, MissingServletRequestParameterException {
		final ObjectMapper objectMapper = new ObjectMapper();
		Role newRole = new Role(1L, "Prueba", true, 1L, new Date(), 1L, new Date(), null, null);
		
		when(roleManagService.updateRole(1L, newRole, token)).thenThrow(ResourceNotFoundException.class);
		
		try {
			mvc.perform(put("/roles/1").contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(newRole)).header("Authorization", "Bearer "+token))
			.andExpect(status().isNotFound());
		} catch (Exception e) {
			log.info(e.toString());
		}
	}
	@Test
	void updateRoleMissingRequestTest() throws ResourceNotFoundException, PrivilegeServiceGenericException, MissingServletRequestParameterException {
		final ObjectMapper objectMapper = new ObjectMapper();
		Role newRole = new Role(1L, "Prueba", true, 1L, new Date(), 1L, new Date(), null, null);
		
		when(roleManagService.updateRole(1L, newRole, token)).thenThrow(MissingServletRequestParameterException.class);
		
		try {
			mvc.perform(put("/roles/1").contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(newRole)).header("Authorization", "Bearer "+token))
			.andExpect(status().isBadRequest());
		} catch (Exception e) {
			log.info(e.toString());
		}
	}
}
