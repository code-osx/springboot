package mx.minsait.academy.privileges.entity;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.jupiter.api.Test;

class RecordTest {
	@Test
	void create() throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String dateInString = "2022-02-14";
		Date fecha = sdf.parse(dateInString);
		
		Record record = new Record();
		record.setId(1L);
		record.setIdUser(1L);
		record.setMethod("MyMethod()");
		record.setDateExc(fecha);
		record.setExecTime(0L);
		
		
		assertEquals(1L, record.getId(), "Se esperaba equivalencia");
		assertEquals(1L, record.getIdUser(), "Se esperaba equivalencia");
		assertEquals("MyMethod()", record.getMethod(), "Se esperaba equivalencia");
		assertEquals(fecha, record.getDateExc(), "Se esperaba equivalencia");
		assertEquals(0L, record.getExecTime(), "Se esperaba equivalencia");
	}
}
