package mx.minsait.academy.privileges.entity;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;

class UserTest {
	@Test
	void create() {
		List<Role> arrayListRole = new ArrayList<>();
		User user = new User();
		user.setId(1L);
		user.setUserName("oscar");
		user.setEmail("example@correo.com");
		user.setPassword("pass123");
		user.setRoles(arrayListRole);
		
		
		assertEquals(1L, user.getId(), "Se esperaba equivalencia");
		assertEquals("oscar", user.getUserName(), "Se esperaba equivalencia");
		assertEquals("example@correo.com", user.getEmail(), "Se esperaba equivalencia");
		assertEquals("pass123", user.getPassword(), "Se esperaba equivalencia");
		assertEquals(arrayListRole, user.getRoles(), "Se esperaba equivalencia");
	}
	
	
}
