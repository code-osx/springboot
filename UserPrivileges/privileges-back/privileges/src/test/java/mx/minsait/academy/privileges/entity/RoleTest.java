package mx.minsait.academy.privileges.entity;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;

class RoleTest {
	@Test
	void create() throws ParseException {
		List<Privilege> arrayListPrivilege = new ArrayList<>();
		List<User> arrayListUser = new ArrayList<>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String dateInString = "2022-02-14";
		Date fecha = sdf.parse(dateInString);
		
		Role role = new Role();
		role.setId(1L);
		role.setName("RoleExample");
		role.setStatus(true);
		role.setIdUserRecord(1L);
		role.setDateRecord(fecha);
		role.setIdUserUpdate(1L);
		role.setDateUpdate(fecha);
		role.setPrivileges(arrayListPrivilege);
		role.setUsers(arrayListUser);
		
		
		assertEquals(1L, role.getId(), "Se esperaba equivalencia");
		assertEquals("RoleExample", role.getName(), "Se esperaba equivalencia");
		assertEquals(true, role.getStatus(), "Se esperaba equivalencia");
		assertEquals(1L, role.getIdUserRecord(), "Se esperaba equivalencia");
		assertEquals(fecha, role.getDateRecord(), "Se esperaba equivalencia");
		assertEquals(1L, role.getIdUserUpdate(), "Se esperaba equivalencia");
		assertEquals(fecha, role.getDateUpdate(), "Se esperaba equivalencia");
		assertEquals(arrayListPrivilege, role.getPrivileges(), "Se esperaba equivalencia");
		assertEquals(arrayListUser, role.getUsers(), "Se esperaba equivalencia");
	}
}
