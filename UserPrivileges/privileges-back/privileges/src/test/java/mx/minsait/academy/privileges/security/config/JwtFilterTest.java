package mx.minsait.academy.privileges.security.config;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;

import java.io.IOException;

import javax.servlet.ServletException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockFilterChain;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.core.userdetails.UserDetails;

import mx.minsait.academy.privileges.config.security.JwtFilter;
import mx.minsait.academy.privileges.config.security.JwtUtil;
import mx.minsait.academy.privileges.service.impl.UserService;

@ExtendWith(MockitoExtension.class)
class JwtFilterTest {
	@Mock
	private JwtUtil jwtUtil = mock(JwtUtil.class);
	
	@Mock
	private UserService service;

	@InjectMocks
    private JwtFilter jwtFilter;
	
	private String token;
	private String testUri;

	@BeforeEach
	void setUp() {
		MockitoAnnotations.initMocks(this);
		token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJvc2NhciIsImlkIjoxLCJleHAiOjE2NDM4MTg2NzEsImlhdCI6MTY0MzIxMzg3MX0.E38QXhevAL-tGf41GejBMregVeejlEktgq7dI_7PiIo";
		testUri = "/testUri";
	}
	
	@Test
	void doFilterInternalTest() throws ServletException, IOException {
		MockHttpServletRequest  request = new MockHttpServletRequest();
		request.addHeader(token, token);
		request.setRequestURI(testUri);
		MockHttpServletResponse  response = new MockHttpServletResponse();
		MockFilterChain filterChain = new MockFilterChain();
	    
	    UserDetails userDetails = mock(UserDetails.class);
	    
	    lenient().when(jwtUtil.extractUsername(token)).thenReturn("oscar");
	    lenient().when(service.loadUserByUsername("oscar")).thenReturn(userDetails);
	    lenient().when(jwtUtil.validateToken(token, userDetails)).thenReturn(true);
	    
	    jwtFilter.doFilter(request, response, filterChain);
	    
	    assertEquals(HttpStatus.OK.value(), response.getStatus());
	}

}
