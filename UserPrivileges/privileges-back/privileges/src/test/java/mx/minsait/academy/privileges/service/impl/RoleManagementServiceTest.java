package mx.minsait.academy.privileges.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.web.bind.MissingServletRequestParameterException;

import mx.minsait.academy.privileges.config.security.JwtUtil;
import mx.minsait.academy.privileges.controller.RoleManagementController;
import mx.minsait.academy.privileges.entity.Role;
import mx.minsait.academy.privileges.model.PrivilegeServiceGenericException;
import mx.minsait.academy.privileges.model.ResourceNotFoundException;
import mx.minsait.academy.privileges.repository.RoleRepository;

@ExtendWith(MockitoExtension.class)
class RoleManagementServiceTest {
	
	@Mock
	private RoleRepository roleRepository;
	
	@Mock
	private JwtUtil jwtUtil = mock(JwtUtil.class);
	
	@InjectMocks
    private RoleManagementService roleManagService;
	
	List<Role> rolesEsperados = new ArrayList<Role>();
	String token;
	
	@BeforeEach
	void setUp() {
		MockitoAnnotations.initMocks(this);
		
		rolesEsperados.add(new Role(1L, "Admin", true, 1L, new Date(), 1L, new Date(), null, null));
		rolesEsperados.add(new Role(2L, "User", true, 2L, new Date(), 2L, new Date(), null, null));
		
		token = "token-test";
	}
	
	@Test
	void getRolesTest() throws PrivilegeServiceGenericException {
		when(roleRepository.findByStatusTrue()).thenReturn(rolesEsperados);
		
		CollectionModel<EntityModel<Role>> rolesReales = roleManagService.getRoles();
		
		assertEquals(2, rolesReales.getContent().size());
	}
	
	@Test
	void getRoleTest() throws ResourceNotFoundException, PrivilegeServiceGenericException{
		Optional<Role> roleOp = Optional.of(rolesEsperados.get(0));
		when(roleRepository.findById(1L)).thenReturn(roleOp);
		
		EntityModel<Optional<Role>> roleReal = roleManagService.getRole(1L);
		Optional<Role> roleEnt = roleReal.getContent();
		
		assertEquals("Admin", roleEnt.get().getName());
	}
	
	@Test
	void createRoleTest() throws MissingServletRequestParameterException, ResourceNotFoundException, PrivilegeServiceGenericException {
		Role newRole = new Role(1L, "Prueba", true, 1L, new Date(), 1L, new Date(), null, null);
		
		lenient().when(jwtUtil.extractIdUser(token)).thenReturn(1L);
		lenient().when(roleRepository.save(newRole)).thenReturn(newRole);
		
		try {
			assertThat(roleManagService.createRole(newRole, token))
			.isEqualTo(EntityModel.of(newRole,  
					linkTo(methodOn(RoleManagementController.class).getRole(newRole.getId())).withSelfRel(),
					linkTo(methodOn(RoleManagementController.class).getRoles()).withRel("roles")));
	    } catch (NullPointerException ex) {
	        assertTrue(ex instanceof NullPointerException);
	    }
		
	}
	
	@Test
	void deleteRoleTest() throws ResourceNotFoundException, PrivilegeServiceGenericException {
		Optional<Role> editRole = Optional.ofNullable(new Role(1L, "Prueba", true, 1L, new Date(), 1L, new Date(), null, null));
		EntityModel<Role> esp = EntityModel.of(
				editRole.get(),
				linkTo(methodOn(RoleManagementController.class).getRole(editRole.get().getId())).withSelfRel(),
				linkTo(methodOn(RoleManagementController.class).getRoles()).withRel("roles"));
		
		lenient().when(jwtUtil.extractIdUser(token)).thenReturn(1L);
		when(roleRepository.findById(1L)).thenReturn(editRole);
		when(roleRepository.save(editRole.get())).thenReturn(editRole.get());
		
		EntityModel<Role> resp = roleManagService.deleteRole(1L, token);
		assertEquals(esp, resp);
	}
	
	@Test
	void updateRoleTest() throws ResourceNotFoundException, PrivilegeServiceGenericException {
		Optional<Role> editRole = Optional.ofNullable(new Role(1L, "Prueba", false, 1L, new Date(), 1L, new Date(), null, null));
		EntityModel<Role> esp = EntityModel.of(
				editRole.get(),
				linkTo(methodOn(RoleManagementController.class).getRole(editRole.get().getId())).withSelfRel(),
				linkTo(methodOn(RoleManagementController.class).getRoles()).withRel("roles"));
		
		lenient().when(jwtUtil.extractIdUser(token)).thenReturn(1L);
		when(roleRepository.findById(1L)).thenReturn(editRole);
		when(roleRepository.save(editRole.get())).thenReturn(editRole.get());
		
		EntityModel<Role> resp = roleManagService.deleteRole(1L, token);
		assertEquals(esp, resp);
	}
	
}
