package mx.minsait.academy.apigategay;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class ApiGatewayConfiguration {
	
	@Bean
	public RouteLocator gatewayRouter(RouteLocatorBuilder builder) {
		return builder.routes()
				.route(p -> p.path("/user/**")
						.uri("lb://auth"))
				.route(p -> p.path("/roles/**")
						.uri("lb://privileges"))
				.route(p -> p.path("/users/**")
						.uri("lb://privileges"))
				.build();
	}
}