package mx.minsait.academy.auth.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import mx.minsait.academy.auth.model.AuthErrorException;
import mx.minsait.academy.auth.model.DefaultError;
import mx.minsait.academy.auth.model.DefaultErrorList;
import mx.minsait.academy.auth.util.ErrorEnum;


/**
 * @author David Gonzalez
 * 
 * Esta clase se encarga de servir como apoyo al controller, manejando de manera desacoplada 
 * las excepciones esperadas en la aplicacion, y manejando el catalogo de errores con ayuda de un enumerador personalizado.
 * Tambien tiene un manejo de errores genericos.
 */

@ControllerAdvice
public class AuthExceptionHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(AuthExceptionHandler.class);
    
    /**
     * Manejo de excepciones de validacion de argumentos de entrada
     * @param pe Excepcion de tipo MethodArgumentNotValidException
     * @return La entidad de respuesta que maneja el error como objeto
     */
	@ExceptionHandler(value = {MethodArgumentNotValidException.class})
	public ResponseEntity<DefaultErrorList> handleValidationExceptionA(MethodArgumentNotValidException pe) {
    	LOGGER.warn("Argmentos invalidos", pe);
		return new ResponseEntity<>(new DefaultErrorList(new DefaultError(ErrorEnum.EXC_ERROR_PARAMS, pe.getLocalizedMessage())), HttpStatus.BAD_REQUEST);

	}

	/**
     * Manejo de excepciones de validacion de formatos de numeros de entrada
     * @param pe Excepcion de tipo NumberFormatException
     * @return La entidad de respuesta que maneja el error como objeto
     */
	@ExceptionHandler(value = {NumberFormatException.class})
	public ResponseEntity<DefaultErrorList> handleValidationExceptionB(NumberFormatException pe) {
    	LOGGER.warn("Excepcion de formatos de numeros de entrada", pe);
		return new ResponseEntity<>(new DefaultErrorList(new DefaultError(ErrorEnum.EXC_ERROR_PARAMS, pe.getLocalizedMessage())), HttpStatus.BAD_REQUEST);

	}

	/**
     * Manejo de excepciones de validacion de tipo de datos de entrada
     * @param pe Excepcion de tipo MethodArgumentTypeMismatchException
     * @return La entidad de respuesta que maneja el error como objeto
     */
	@ExceptionHandler(value = {MethodArgumentTypeMismatchException.class})
	public ResponseEntity<DefaultErrorList> handleValidationExceptionC(MethodArgumentTypeMismatchException pe) {
    	LOGGER.warn("Excepcion de tipo de datos de entrada", pe);
		return new ResponseEntity<>(new DefaultErrorList(new DefaultError(ErrorEnum.EXC_ERROR_PARAMS, pe.getLocalizedMessage())), HttpStatus.BAD_REQUEST);

	}


	/**
     * Manejo de excepciones de validacion de cantidad de parametros de entrada enviados
     * @param pe Excepcion de tipo MissingServletRequestParameterException
     * @return La entidad de respuesta que maneja el error como objeto
     */
	@ExceptionHandler(value = {MissingServletRequestParameterException.class})
	public ResponseEntity<DefaultErrorList> handleValidationExceptionD(MissingServletRequestParameterException pe) {
    	LOGGER.warn("Excepcion de cantidad de parametros de entrada enviados", pe);
		return new ResponseEntity<>(new DefaultErrorList(new DefaultError(ErrorEnum.EXC_ERROR_PARAMS, pe.getLocalizedMessage())), HttpStatus.BAD_REQUEST);
	}
	
	/**
	 * Manejo de recursos no encontrados (Auth)
	 * @param ResourceNotFoundException
	 * @return La entidad de respuesta que maneja el error como objeto
	 */
    @ExceptionHandler(AuthErrorException.class)
    public ResponseEntity<DefaultErrorList> handleAuthErrorException(AuthErrorException ex) {
		LOGGER.error("Error en la ejecucion [handleAuthErrorException]", ex);
		return new ResponseEntity<>(new DefaultErrorList(new DefaultError(ErrorEnum.EXC_ERROR_AUTH)), HttpStatus.NOT_FOUND);
    }
    
	/**
	 * Manejo de excepcion generica
	 * @param ex Excepcion generica de tipo Exception
	 * @return La entidad de respuesta que maneja el error como objeto
	 */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<DefaultErrorList> handleGenericException(Exception ex) {
		LOGGER.error("Error en la ejecucion [handleGenericException]", ex);
		return new ResponseEntity<>(new DefaultErrorList(new DefaultError(ErrorEnum.EXC_GENERICO)), HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
