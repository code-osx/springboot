package mx.minsait.academy.auth.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mx.minsait.academy.auth.entity.User;
import mx.minsait.academy.auth.model.AuthErrorException;
import mx.minsait.academy.auth.model.AuthRequest;
import mx.minsait.academy.auth.service.UserService;
import mx.minsait.academy.auth.util.JwtUtil;
import net.minidev.json.JSONObject;

/**
 * Minsait Controlador: UserController.java 
 * Descripción: Controlador encargado de recibir las peticiones REST e invocar el servicio
 * correspondiente.
 */
@RestController
@RequestMapping("/user")
public class UserController {
	
	private UserService userService;
	User userRes;

	@Autowired
	JwtUtil jwtUtil;

	UserController(UserService userService) {
		this.userService = userService;
	}
	
	/**
	 * Método encargado de obtener el token del usuario
	 * @url: /users/auth
	 * @return ResponseEntity<>
	 * @throws Exception 
	 */
	@PostMapping("/auth")
    public ResponseEntity<Object> generateToken(@RequestBody AuthRequest authRequest) {
		boolean isValid = false;
        try {
            userRes = userService.getUserByName(authRequest.getUserName());
            if(new BCryptPasswordEncoder().matches(authRequest.getPassword(), userRes.getPassword())){
            	isValid = true;
            }
            else{
            	 throw new AuthErrorException("El usuario o la contraseña son incorrectos.");
            }
            
        } catch (Exception ex) {
            throw new AuthErrorException("El usuario o la contraseña son incorrectos.");
        }
        
        JSONObject entity = new JSONObject();
        if (isValid) {
        	entity.put("token", "Bearer "+jwtUtil.generateToken(authRequest.getUserName(), userRes.getId()));
        }
        
        return new ResponseEntity<>(entity, HttpStatus.OK);
    }

}
