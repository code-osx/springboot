package mx.minsait.academy.auth.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import mx.minsait.academy.auth.entity.User;
import mx.minsait.academy.auth.model.ResourceNotFoundException;
import mx.minsait.academy.auth.repository.UserRepository;

/**
* Minsait
* Servicio: UserService.java
* Descripción: Servicio que se encarga de comunicarse con el repositorio y validar el token recibido en el header "Authorization".
* También es utilizado para obtener todos los usuarios y un usuario en especifico con su respectivo rol.
*/

@Service
@Slf4j
public class UserService implements UserDetailsService {
    @Autowired
    private UserRepository repository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = repository.findByUserName(username);
        return new org.springframework.security.core.userdetails.User(user.getUserName(), user.getPassword(), new ArrayList<>());
    }
    
    /**
	* Método encargado de obtener uno de los usuarios de la base de datos con el nombre de ususario y password como referencia
	* @param id
	* @return EntityModel<Optional<User>>
	* @throws ResourceNotFoundException 
	*/
	public User getUserByName(String name) throws ResourceNotFoundException {
		User user = repository.findByUserName(name);
		
		if(user != null) {
			log.info("Service: Se encontró el usuario");
			return user;
			
		} else {
			log.info("Service: No se encontró el usuario");
			throw new ResourceNotFoundException("No se encontro el usuario");
		}
	}
	
}
