package mx.minsait.academy.auth.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
* Minsait
* Entidad: User.java
* Descripción: Entidad utilizada para validar el token que obtenemos del header "Authorization".
* Entidad utilizada para persistir la informacion de un Usuario. 
* Uso de lombok para no especificar setters, getters y constructor.
*/

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name="USER_AUTH")
public class User {
    @Id
    private long id;
    private String userName;
    @JsonIgnore
    private String password;
    private String email;
}
