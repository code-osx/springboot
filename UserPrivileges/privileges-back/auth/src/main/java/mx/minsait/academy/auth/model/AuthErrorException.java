package mx.minsait.academy.auth.model;

public class AuthErrorException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public AuthErrorException(String msg) {
		super(msg);
	}
}
