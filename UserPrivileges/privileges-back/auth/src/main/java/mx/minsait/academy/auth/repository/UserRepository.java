package mx.minsait.academy.auth.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import mx.minsait.academy.auth.entity.User;


/**
* Minsait
* Repositorio: UserRepository.java
* Descripción: Interfaz utilizada para validar el token que obtenemos del header "Authorization".
* También persiste la información de User
*/

public interface UserRepository extends JpaRepository<User,Long> {
    User findByUserName(String username);
    User findByUserNameAndPassword(String username, String password);    
}
